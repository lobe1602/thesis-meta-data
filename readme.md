# DT133G - Final Project meta data

This repository contains meta data for my final project for BSc Software Engineering

## Contents

The file [lobe1602_thesis-meta-data.zip](lobe1602_thesis-meta-data.zip) contains

- Rejoinder
- Publishing conditions
- Thesis meta data (Section 8 uncomplete)

## Contributions guideline

None required

## Contact

[Bernard Che Longho](mailto:lobe1602@student.miun.se)
